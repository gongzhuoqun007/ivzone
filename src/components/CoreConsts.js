/**
 * 系统全局常量配置
 */
export default {
    PageSize: 'size', // 分页条数字段
    PageCurrent: 'current', // 分页页数

    DefaultPID: 'pid',
    DefaultRowKey: 'id', // 整个系统的默认rowKey
    SuccessCode: 200, // 默认成功码
    ConfirmTitle: '操作提示', // 默认确认标题
    ConfirmContent: '确认要执行操作?', // 默认确认内容
    DelSuccessMsg: '删除记录成功', // 默认删除成功提示, 以后台返回的消息为准
    DelConfirmTitle: '删除提示', // 默认删除提示框标题
    DelConfirmContent: '确定删除此记录吗？', // 默认删除提示框内容
    SubmitSuccessMsg: '数据提交成功', // 默认数据提交成功提示
    OtherOperaSuccessMsg: '操作成功', // 其他动作操作成功的提示
}